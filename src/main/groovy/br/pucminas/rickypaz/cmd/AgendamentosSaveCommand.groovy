package br.pucminas.rickypaz.cmd

import br.pucminas.rickypaz.Agendamento
import br.pucminas.rickypaz.Ativo
import grails.databinding.BindUsing
import grails.validation.Validateable

import java.time.LocalDate

class AgendamentosSaveCommand implements Validateable {

    @BindUsing({obj, source ->
        if (!source['agendamentos'])
            return []

        def list = source['agendamentos'].collect {
            new Agendamento(data: LocalDate.parse(it.data), ativo: Ativo.findByCodigo(it.codigoAtivo))
        }
        return list
    })
    List<Agendamento> agendamentos

}
