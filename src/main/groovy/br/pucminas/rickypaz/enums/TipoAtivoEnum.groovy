package br.pucminas.rickypaz.enums

enum TipoAtivoEnum {

    EQUIPAMENTO,
    MAQUINARIO,
    SENSOR,
    DIVERSOS

}