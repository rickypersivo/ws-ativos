package br.pucminas.rickypaz

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class AtivoServiceSpec extends Specification {

    AtivoService ativoService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new Ativo(...).save(flush: true, failOnError: true)
        //new Ativo(...).save(flush: true, failOnError: true)
        //Ativo ativo = new Ativo(...).save(flush: true, failOnError: true)
        //new Ativo(...).save(flush: true, failOnError: true)
        //new Ativo(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ativo.id
    }

    void "test get"() {
        setupData()

        expect:
        ativoService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Ativo> ativoList = ativoService.list(max: 2, offset: 2)

        then:
        ativoList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ativoService.count() == 5
    }

    void "test delete"() {
        Long ativoId = setupData()

        expect:
        ativoService.count() == 5

        when:
        ativoService.delete(ativoId)
        sessionFactory.currentSession.flush()

        then:
        ativoService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        Ativo ativo = new Ativo()
        ativoService.save(ativo)

        then:
        ativo.id != null
    }
}
