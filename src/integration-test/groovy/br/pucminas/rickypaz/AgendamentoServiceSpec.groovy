package br.pucminas.rickypaz

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class AgendamentoServiceSpec extends Specification {

    AgendamentoService agendamentoService
    SessionFactory sessionFactory

    private void setupData() {
        new Ativo(codigo: "ATIVO001", nome: "Ativo").save(flush: true)
    }

    void "test get"() {
        setupData()

        expect:
        agendamentoService.get(1) != null
    }

    void "test salvar agendamentos em lote"() {
        given:
        setupData()
        Ativo ativo = Ativo.first()
        Agendamento agendamento1 = new Agendamento(ativo: ativo)
        Agendamento agendamento2 = new Agendamento(ativo: ativo)
        Agendamento agendamento3 = new Agendamento(ativo: ativo)

        when:
        def agendamentos = [agendamento1, agendamento2, agendamento3]
        agendamentoService.save(agendamentos)
        sessionFactory.currentSession.flush()

        then:
        Agendamento.countByAtivo(ativo) == 3
    }

    void "test list"() {
        setupData()

        when:
        List<Agendamento> agendamentoList = agendamentoService.list(max: 2, offset: 2)

        then:
        agendamentoList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        agendamentoService.count() == 5
    }

    void "test delete"() {
        Long agendamentoId = setupData()

        expect:
        agendamentoService.count() == 5

        when:
        agendamentoService.delete(agendamentoId)
        sessionFactory.currentSession.flush()

        then:
        agendamentoService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        Agendamento agendamento = new Agendamento()
        agendamentoService.save(agendamento)

        then:
        agendamento.id != null
    }
}
