package ws.ativos

import br.pucminas.rickypaz.Ativo
import br.pucminas.rickypaz.Role
import br.pucminas.rickypaz.User
import br.pucminas.rickypaz.UserRole
import br.pucminas.rickypaz.enums.TipoAtivoEnum

class BootStrap {

    def init = { servletContext ->
        User user
        Role role

        User.withTransaction {
            if (User.count() == 0) {
                user = new User(username: "APP-0001", password: "CONFIDENTIAL", enabled: true, accountExpired: false, accountLocked: false, passwordExpired: false).save(flush: true)
                role = new Role(authority: "ROLE_ATIVO").save(flush: true)
                new UserRole(user: user, role: role).save(flush: true)

                user = new User(username: "APP-0002", password: "CONFIDENTIAL", enabled: true, accountExpired: false, accountLocked: false, passwordExpired: false).save(flush: true)
                role = new Role(authority: "ROLE_MANUTENCAO").save(flush: true)
                new UserRole(user: user, role: role).save(flush: true)
            }
        }

        Ativo.withTransaction {
            if (Ativo.count() == 0) {
                TipoAtivoEnum[] tipoAtivosArr = TipoAtivoEnum.values()
                TipoAtivoEnum tipoAtivo

                1.upto(100, {
                    tipoAtivo = tipoAtivosArr[(Math.random() * (tipoAtivosArr.size() - 1)).toInteger()]
                    new Ativo(codigo: "AT-${it}", nome: "AT-${it}", tipo: tipoAtivo).save(flush: true)
                })
            }
        }
    }
    def destroy = {
    }
}
