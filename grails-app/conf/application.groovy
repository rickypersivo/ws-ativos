

// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.useBasicAuth = true
grails.plugin.springsecurity.basic.realmName = "WS ATIVOS"
grails.plugin.springsecurity.userLookup.userDomainClassName = 'br.pucminas.rickypaz.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'br.pucminas.rickypaz.UserRole'
grails.plugin.springsecurity.authority.className = 'br.pucminas.rickypaz.Role'
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
	[pattern: '/',               access: ['permitAll']],
	[pattern: '/error',          access: ['permitAll']],
	[pattern: '/index',          access: ['permitAll']],
	[pattern: '/index.gsp',      access: ['permitAll']],
	[pattern: '/shutdown',       access: ['permitAll']],
	[pattern: '/assets/**',      access: ['permitAll']],
	[pattern: '/**/js/**',       access: ['permitAll']],
	[pattern: '/**/css/**',      access: ['permitAll']],
	[pattern: '/**/images/**',   access: ['permitAll']],
	[pattern: '/**/favicon.ico', access: ['permitAll']],
	[pattern: '/ativo/**', 		 access: ['ROLE_ATIVO']],
	[pattern: '/agendamento/**', access: ['ROLE_MANUTENCAO']],
	[pattern: '/manutencao/**',  access: ['ROLE_MANUTENCAO']]
]

grails.plugin.springsecurity.filterChain.chainMap = [
	[pattern: '/assets/**',      filters: 'none'],
	[pattern: '/**/js/**',       filters: 'none'],
	[pattern: '/**/css/**',      filters: 'none'],
	[pattern: '/**/images/**',   filters: 'none'],
	[pattern: '/**/favicon.ico', filters: 'none'],
	[pattern: '/**',             filters: 'JOINED_FILTERS']
]

