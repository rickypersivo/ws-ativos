package br.pucminas.rickypaz

import br.pucminas.rickypaz.cmd.AgendamentosSaveCommand
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.CREATED
import static org.springframework.http.HttpStatus.NOT_FOUND
import static org.springframework.http.HttpStatus.NO_CONTENT
import static org.springframework.http.HttpStatus.OK
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY

import grails.gorm.transactions.ReadOnly
import grails.gorm.transactions.Transactional

@ReadOnly
class AgendamentoController {

    AgendamentoService agendamentoService

    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", saveAll: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond agendamentoService.list(params), model:[agendamentoCount: agendamentoService.count()]
    }

    def show(Long id) {
        respond agendamentoService.get(id)
    }

    @Transactional
    def save(Agendamento agendamento) {
        if (agendamento == null) {
            render status: NOT_FOUND
            return
        }
        if (agendamento.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond agendamento.errors
            return
        }

        try {
            agendamentoService.save(agendamento)
        } catch (ValidationException e) {
            respond agendamento.errors
            return
        }

        respond agendamento, [status: CREATED, view:"show"]
    }

    @Transactional
    def saveAll(AgendamentosSaveCommand agendamentosSaveCommand) {
        if (agendamentosSaveCommand.agendamentos == null || agendamentosSaveCommand.agendamentos.size() == 0) {
            render status: NOT_FOUND
            return
        }
        if (agendamentosSaveCommand.agendamentos.find { it.hasErrors() }) {
            transactionStatus.setRollbackOnly()
            def errors = []
            agendamentosSaveCommand.agendamentos.findAll { it.hasErrors() }.each { errors.addAll(it.errors) }
            respond errors
            return
        }

        try {
            agendamentoService.save(agendamentosSaveCommand.agendamentos)
        } catch (ValidationException e) {
            respond e
            return
        } catch(e) {
            e.printStackTrace()
        }

        respond agendamentosSaveCommand.agendamentos, [status: CREATED, view:"saveAll"]
    }

    @Transactional
    def update(Agendamento agendamento) {
        if (agendamento == null) {
            render status: NOT_FOUND
            return
        }
        if (agendamento.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond agendamento.errors
            return
        }

        try {
            agendamentoService.save(agendamento)
        } catch (ValidationException e) {
            respond agendamento.errors
            return
        }

        respond agendamento, [status: OK, view:"show"]
    }

    @Transactional
    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        agendamentoService.delete(id)

        render status: NO_CONTENT
    }
}
