package br.pucminas.rickypaz

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.CREATED
import static org.springframework.http.HttpStatus.NOT_FOUND
import static org.springframework.http.HttpStatus.NO_CONTENT
import static org.springframework.http.HttpStatus.OK
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY

import grails.gorm.transactions.ReadOnly
import grails.gorm.transactions.Transactional

@ReadOnly
class ManutencaoController {

    ManutencaoService manutencaoService

    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond manutencaoService.list(params), model:[manutencaoCount: manutencaoService.count()]
    }

    def show(Long id) {
        respond manutencaoService.get(id)
    }

    @Transactional
    def save(Manutencao manutencao) {
        if (manutencao == null) {
            render status: NOT_FOUND
            return
        }
        if (manutencao.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond manutencao.errors
            return
        }

        try {
            manutencaoService.save(manutencao)
        } catch (ValidationException e) {
            respond manutencao.errors
            return
        }

        respond manutencao, [status: CREATED, view:"show"]
    }

    @Transactional
    def update(Manutencao manutencao) {
        if (manutencao == null) {
            render status: NOT_FOUND
            return
        }
        if (manutencao.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond manutencao.errors
            return
        }

        try {
            manutencaoService.save(manutencao)
        } catch (ValidationException e) {
            respond manutencao.errors
            return
        }

        respond manutencao, [status: OK, view:"show"]
    }

    @Transactional
    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        manutencaoService.delete(id)

        render status: NO_CONTENT
    }
}
