package br.pucminas.rickypaz

import br.pucminas.rickypaz.enums.TipoAtivoEnum
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.CREATED
import static org.springframework.http.HttpStatus.NOT_FOUND
import static org.springframework.http.HttpStatus.NO_CONTENT
import static org.springframework.http.HttpStatus.OK

import grails.gorm.transactions.ReadOnly
import grails.gorm.transactions.Transactional

@ReadOnly
class AtivoController {

    AtivoService ativoService

    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond ativoService.list(params), model:[ativoCount: ativoService.count()]
    }

    def listByCodigos() {
        List<String> codigos = params.list('codigos')
        respond ativoService.findAllByCodigoInList(codigos), model:[ativoCount: ativoService.count()]
    }

    def findAllByTipoAtivo(String tipoAtivo) {
        respond ativoService.findAllByTipo(tipoAtivo as TipoAtivoEnum), model:[ativoCount: ativoService.count()]
    }

    def show(Long id) {
        respond ativoService.get(id)
    }

    def view(String codigo) {
        respond ativoService.get(codigo)
    }

    @Transactional
    def save(Ativo ativo) {
        println ativo
        if (ativo == null) {
            render status: NOT_FOUND
            return
        }
        if (ativo.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond ativo.errors
            return
        }

        try {
            ativoService.save(ativo)
        } catch (ValidationException e) {
            respond ativo.errors
            return
        }

        respond ativo, [status: CREATED, view:"show"]
    }

    @Transactional
    def update(Ativo ativo) {
        if (ativo == null) {
            render status: NOT_FOUND
            return
        }
        if (ativo.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond ativo.errors
            return
        }

        try {
            ativoService.save(ativo)
        } catch (ValidationException e) {
            respond ativo.errors
            return
        }

        respond ativo, [status: OK, view:"show"]
    }

    @Transactional
    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        ativoService.delete(id)

        render status: NO_CONTENT
    }
}
