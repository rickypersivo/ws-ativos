package br.pucminas.rickypaz

import br.pucminas.rickypaz.enums.TipoAtivoEnum

class Ativo {

    String codigo
    String nome
    TipoAtivoEnum tipo

    static constraints = {
        codigo unique: true
    }
}
