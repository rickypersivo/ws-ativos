package br.pucminas.rickypaz

import java.time.LocalDate

class Manutencao {

    String responsible
    LocalDate date

    static belongsTo = [ativo: Ativo, agendamento: Agendamento]

    static constraints = {
        agendamento nullable: true
    }
}
