package br.pucminas.rickypaz

import java.time.LocalDate

class Agendamento {

    String id
    LocalDate data

    static belongsTo = [ativo: Ativo]

    static constraints = {
    }

    static mapping = {
        id generator: 'uuid'
    }
}
