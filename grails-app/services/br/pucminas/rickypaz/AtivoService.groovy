package br.pucminas.rickypaz

import br.pucminas.rickypaz.enums.TipoAtivoEnum
import grails.gorm.services.Service

@Service(Ativo)
interface AtivoService {

    Ativo get(Serializable id)

    Ativo get(String codigo)

    List<Ativo> findAllByCodigoInList(List<String> codigos)

    List<Ativo> list(Map args)

    List<Ativo> findAllByTipo(TipoAtivoEnum tipoAtivo)

    Long count()

    void delete(Serializable id)

    Ativo save(Ativo ativo)

}