package br.pucminas.rickypaz

import grails.gorm.services.Service

@Service(Agendamento)
abstract class AgendamentoService {

    abstract Agendamento get(Serializable id)

    abstract List<Agendamento> list(Map args)

    abstract Long count()

    abstract void delete(Serializable id)

    abstract Agendamento save(Agendamento agendamento)

    void save(List<Agendamento> agendamentos) {
        agendamentos.each {
            this.save(it)
        }
    }

}